#pragma once

#include <Arduino.h>


class motor

{


    private:
int Pin_PWM_FWD;
int PIN_PWM_BAK;
int PIN_ENABLE;

int Dir = 0;
signed int Trimm=0;

    public:

 motor(int _Pin_PWM_FWD, int _PIN_PWM_BAK,int _PIN_ENABLE);

void stop();
void start(int Power, int direction);
void setTrimm(signed int _trimm);


};