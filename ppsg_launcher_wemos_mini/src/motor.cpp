#include "arduino.h"
#include "motor.h"

motor::motor(int _Pin_PWM_FWD, int _PIN_PWM_BAK, int _PIN_ENABLE)
{
    Pin_PWM_FWD = _Pin_PWM_FWD;
    PIN_PWM_BAK = _PIN_PWM_BAK;
    PIN_ENABLE = _PIN_ENABLE;
    pinMode(PIN_ENABLE, OUTPUT);
    pinMode(PIN_PWM_BAK, OUTPUT);
    pinMode(Pin_PWM_FWD, OUTPUT);
}

void motor::setTrimm(signed int _trimm)
{
    Trimm = _trimm;
}

void motor::stop()
{
    digitalWrite(PIN_ENABLE, false);
    analogWrite(Pin_PWM_FWD, 0);
    analogWrite(PIN_PWM_BAK, 0);
}
void motor::start(int Power, int direction)
{
    Power += Trimm;
    long PWM_VAL = map(Power, 0, 100, 0, 1023);
    digitalWrite(PIN_ENABLE, true);
    if (direction == 0)
    {
        analogWrite(Pin_PWM_FWD, PWM_VAL);
        analogWrite(PIN_PWM_BAK, 0);
    }
    else
    {
        analogWrite(Pin_PWM_FWD, 0);
        analogWrite(PIN_PWM_BAK, PWM_VAL);
    }
}
