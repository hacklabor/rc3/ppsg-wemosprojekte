#include <Arduino.h>
#include "motor.h"
int M1_enable = D1;
int M1_PWM_F = D3;
int M1_PWM_B = D2;

int M2_enable = D6;
int M2_PWM_F = D8;
int M2_PWM_B = D7;

long M1_PWM_VAL = 0;
int M1_PWM_PROZENT = 0;
int M1_Dir = 1;
long M2_PWM_VAL = 0;
int M2_PWM_PROZENT = 0;
int M2_Dir = 1;
int M_load_PWM_PIN = D5;
int shootingCycle = 2;
unsigned long lasttime =0;


int Ball_Pin = D4;
int shoot = 0;

int ModeShooting = 0; // 0=Reload;  1=readyToShoot 2=shotDone

String Serial_IN = "";

motor m1(M1_PWM_F, M1_PWM_B, M1_enable);
motor m2(M2_PWM_F, M2_PWM_B, M2_enable);



void SendResponse()
{
  Serial_IN = "";
  String SerialOUT1 = "#";
  SerialOUT1 += "," + String(M1_PWM_PROZENT);
  SerialOUT1 += "," + String(M2_PWM_PROZENT);
  SerialOUT1 += "," + String(M1_Dir);
  SerialOUT1 += "," + String(M2_Dir);
  SerialOUT1 += "," + String(shootingCycle);
  SerialOUT1 += "," + String("#");
  Serial.println(SerialOUT1);
}

boolean GetSerial()
{
  while (Serial.available())
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    Serial_IN += inChar;

    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n')
    {
      return true;
    }
  }
  return false;
}

void setup()
{
  // put your setup code here, to run once:

  pinMode(M_load_PWM_PIN, OUTPUT);
  pinMode(Ball_Pin, INPUT_PULLUP);
  analogWriteFreq(20000);
  m1.stop();
  m2.stop();
  digitalWrite(M_load_PWM_PIN, LOW);

  Serial_IN.reserve(200);
  Serial.begin(115200);
  delay(1000);

}

void loop()
{

  if (GetSerial())
  {
     
    if (Serial_IN[0] == '#' && Serial_IN[18] == '#') //Check
    {
      //#,030,000,0,0,0,0# (#=start, PWM M1 0-100%, PWM M2 0-100%, DirectionM1=0/1,DirectionM2=0/1, SingleShoot/Rapidfire = 0/1, nur response = 1, #=end
      int OnlyResp = Serial_IN.substring(16, 17).toInt();
      if (OnlyResp == 0)
      {
        M1_PWM_PROZENT = Serial_IN.substring(2, 5).toInt();

        M2_PWM_PROZENT = Serial_IN.substring(6, 9).toInt();

        M1_Dir = Serial_IN.substring(10, 12).toInt();
        M2_Dir = Serial_IN.substring(12, 13).toInt();
        shootingCycle = Serial_IN.substring(14, 15).toInt();

        if (M1_PWM_PROZENT > 0)
        {
          m1.start(M1_PWM_PROZENT, M1_Dir);
          delay(100);
        }
        else
        {
          m1.stop();
        }
        if (M2_PWM_PROZENT > 0)
        {
          m2.start(M2_PWM_PROZENT, M2_Dir);
        }
        else
        {
          m2.stop();
        }
      }
      if (shootingCycle!=2){
         lasttime=millis();

      }
      SendResponse();
    }
  }
  if (digitalRead(Ball_Pin) && shootingCycle==0){
    while (digitalRead(Ball_Pin)==LOW)
    {
      delay(10);
    }
    
    shootingCycle=1;
    
  }
   if (digitalRead(Ball_Pin)==LOW && shootingCycle==1){
    while (digitalRead(Ball_Pin)==HIGH)
    {
      delay(10);
    }
    
    shootingCycle=2;
    
  }
  if (shootingCycle == 2 || ((M1_PWM_PROZENT < 15 && M2_PWM_PROZENT <15)) || ((millis()-lasttime)>10000))
  {
      //Serial.println("stop");
      shootingCycle = 2;
       digitalWrite(M_load_PWM_PIN, LOW);
       delay(1000);
       m1.stop();
       m2.stop();
       //M1_PWM_PROZENT=0;
       //M2_PWM_PROZENT=0;
  }
  else
  {
   
       digitalWrite(M_load_PWM_PIN, HIGH);
      
   
  }
}
