#include <Arduino.h>

// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class
// 10/7/2011 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
//
// Changelog:
//      2013-05-08 - added multiple output formats
//                 - added seamless Fastwire support
//      2011-10-07 - initial release

/* ============================================
I2Cdev device library code is placed under the MIT license
Copyright (c) 2011 Jeff Rowberg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
===============================================
*/

#include "I2Cdev.h"
#include "MPU6050.h"
#include "TimeTrigger.h"
#include <ESP8266WiFi.h>
#include <Wire.h>
#include <PubSubClient.h>
#include "secrets.h"
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

MPU6050 accelgyro;
TimeTrigger Trigger10000ms(10000);
TimeTrigger Trigger1000ms(1000);
TimeTrigger Trigger10ms(10);
WiFiClient espClient;
PubSubClient client(espClient);

int16_t ax, ay, az;
int16_t gx, gy, gz;
long AllPoints = 0;
bool startCount = false;
int reset = 0;

int LED_PIN = D4;
bool blinkState = false;

int16_t getPoints(int16_t wert, int startCounting)
{
  if (wert < 0)
  {
    wert = wert * (-1);
  }

  if (wert > startCounting)
  {
    return wert;
  }
  return 0;
}

long getPoints(long startPoints)
{
  accelgyro.getRotation(&gx, &gy, &gz);

  long points = 0;

  points += getPoints(gx, startPoints);
  points += getPoints(gy, startPoints);
  points += getPoints(gz, startPoints);
  return points;
}


void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    if (client.connect(client_name, mqtt_user, mqtt_password))
    {
      Serial.println("connected");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(1000);
    }
  }
}

void setup_wifi()
{
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}



void setup()
{

  Wire.begin();
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);

  // initialize device
  Serial.println("Initializing I2C devices...");
  accelgyro.initialize();

  // verify connection
  Serial.println("Testing device connections...");
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  pinMode(LED_PIN, OUTPUT);
  accelgyro.CalibrateGyro(30);
  accelgyro.setFullScaleGyroRange(2);
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
      Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR)
      Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR)
      Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR)
      Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR)
      Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop()
{
  ArduinoOTA.handle();
  if (Trigger10000ms.Trigger())
  {
    String tmp = "ALIVE:" + String(client_name);
    Serial.println(tmp);
    client.publish(status_topic, tmp.c_str(), true);
  }

  if (!client.connected())
  {
    reconnect();
  }
  client.loop();
  if (Trigger10ms.Trigger())
  {
    long tmp_points = getPoints(4000);
    if (tmp_points > 0 && startCount == false)
    {
      AllPoints += tmp_points * 0.001;
      startCount = true;
      String tmp = "HIT:" + String(client_name);
      client.publish(status_topic, tmp.c_str(), true);
    }
    if (startCount)
    {
      long tmp_points1 = getPoints(2000);
      if (tmp_points1 > 0)
      {
        AllPoints += tmp_points1 * 0.001;
        startCount = true;
        reset = 0;
      }
      else
      {

        
        reset++;
        if (reset > 200)
        {

          String tmp = "DONE:" + String(client_name);
          client.publish(status_topic, tmp.c_str(), true);
          AllPoints = 0;
          delay(1000);
          startCount = false;
          reset = 0;
        }
      }
    }
  }
  if (Trigger1000ms.Trigger()){
    if (startCount){
      client.publish(points_topic, String(AllPoints).c_str(), true);
    }

  }
}
